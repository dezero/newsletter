# Newsletter module for Dz Framework using Mailchimp

This module provides an easy way to integrate MailChimp with Dz Framework.

The module is based on this [Laravel newsletter package](https://github.com/spatie/laravel-newsletter).

## Installation

Add these lines to composer.json file:

```shell
"require": {
    ...
    "dezero/newsletter": "dev-master"
    ...
},
"repositories":[
    ...
    {
        "type": "vcs",
        "url" : "git@bitbucket.org:dezero/newsletter.git"
    }
    ...
]
```

## Configuration

1) Define the module in the configuration file `/app/config/common/modules.php`
```shell

    // Newsletter module
    'newsletter' => [
        'class' => '\dzlab\newsletter\Module',
        // 'class' => '\newsletter\Module',
    ],
```

2) Add a new alias path in `/app/config/common/aliases.php`
```shell
    'dzlab.newsletter'  => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'dezero' . DIRECTORY_SEPARATOR . 'newsletter',
```

3) Set new component in `/app/config/common/components.php`
```shell
    // Mailchimp component
    'mailchimp' => [
        'class' => '\dzlab\newsletter\components\MailchimpManager'
    ],
```

4) Register a log file for Newsletter module in `/app/config/components/logs.php`
```shell
    // Logs for Newsletter module
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'newsletter.log',
        'categories' => 'newsletter',
    ],
    [
        'class' => '\dz\log\FileLogRoute',
        'logFile' => 'newsletter_dev.log',
        'categories' => 'newsletter_dev',
    ],
```


5) Configurate Mailchimp via environment variables from the `.env` file.
```shell
# The API key of a MailChimp account. See https://us10.admin.mailchimp.com/account/api-key-popup/
MAILCHIMP_APIKEY="REPLACE_ME"

# Default MailChimp list id. See http://kb.mailchimp.com/lists/managing-subscribers/find-your-list-id
MAILCHIMP_LIST_ID="REPLACE_ME"
```

6) Copy file `config/newsletter.php` to your project directory `/app/config/components/newsletter.php`. Open the copied file and add your config values, if needed.

7) Run "yiic migrate up" command in your console

8) If you want to manage Mailchimp subscriptions via REST API, create a new file into API module called `/api/controllers/MailchimpController.php` and paste this content:
```shell
<?php
/*
|--------------------------------------------------------------------------
| Controller class for managing Mailchimp subscriptions
|--------------------------------------------------------------------------
*/

namespace api\controllers;

use dzlab\newsletter\api\controllers\MailchimpController as CoreMailchimpController;
use Yii;

class MailchimpController extends CoreMailchimpController
{
}
```
