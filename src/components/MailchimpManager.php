<?php
/**
 * Mailchimp Manager
 * 
 * Helper classes to work with Mailchimp marketing API
 * 
 * @see https://github.com/spatie/laravel-newsletter
 * @see https://mailchimp.com/developer/marketing/api/
 */

namespace dzlab\newsletter\components;

use DrewM\MailChimp\MailChimp;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\base\ApplicationComponent;
use dzlab\newsletter\models\MailchimpSubscription;
use Yii;

class MailchimpManager extends ApplicationComponent
{
    /**
     * @var array Mailchimp configuration
     */
    protected $vec_config = [];


    /**
     * Test mode (log all responses)
     */
    protected $is_test_mode = false;


    /**
     * @var \DrewM\MailChimp\MailChimp
     */
    protected $mailchimp;


    /**
     * Init function
     */
    public function init()
    {
        parent::init();

        // Mailchimp configuration
        $this->vec_config = Yii::app()->config->get('components.newsletter', 'mailchimp');
        
        // Log all responses (DEV mode)
        if ( isset($this->vec_config['is_test_mode']) )
        {
            $this->is_test_mode = $this->vec_config['is_test_mode'];
        }

        // Init Mailchimp API
        $this->mailchimp = new MailChimp($this->vec_config['api_key']);
        $this->mailchimp->verify_ssl = true;
    }


    /**
     * Return MailChimp API library
     */
    public function get_api()
    {
        return $this->mailchimp;
    }


    /**
     * Get Data Center of Mailchimp used as prefix for some URL's
     */
    public function get_data_center()
    {
        $data_center = '';
        $api_key = $this->vec_config['api_key'];
        if ( !empty($api_key) && strpos($api_key, '-') !== false )
        {
            list(, $data_center) = explode('-', $api_key);
        }

        return $data_center;
    }


    /**
     * Get information about all lists in the account
     */
    public function get_lists($vec_parameters = [])
    {
        // Get lists information via GET request
        $response = $this->mailchimp->get("lists", $vec_parameters);

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('get_lists', $response);

            return false;
        }

        return $response;
    }


    /**
     * Get a Mailchimp configured by name
     */
    public function get_list_id($list_name = '')
    {
        // Get default list name
        if ( empty($list_name) )
        {
            $list_name = $this->vec_config['default_list_name'];
        }

        // Return list_id from config file
        if ( isset($this->vec_config['lists'][$list_name]) && isset($this->vec_config['lists'][$list_name]['id']) )
        {
            return $this->vec_config['lists'][$list_name]['id'];
        }

        return null;
    }


    /**
     * Get a list of all merge fields (formerly merge vars) for a list
     */
    public function get_merge_fields($list_name = '', $vec_parameters = [])
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::get_merge_fields() - List {$list_name} does not exist.");
            
            return false;
        }

        // Get merge fields information via GET request
        $response = $this->mailchimp->get("lists/{$list_id}/merge-fields", $vec_parameters);

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('get_merge_fields', $response);

            return false;
        }

        return $response;
    }


    /**
     * Subscribe email to Mailchimp
     */
    public function subscribe($email, $vec_merge_fields = [], $list_name = '', $vec_options = [], $user_id = null)
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::subscribe() - List {$list_name} does not exist.");
            
            return false;
        }

        // Generate subscription options
        $vec_subscription_options = $this->get_subscription_options($email, $vec_merge_fields, $vec_options);
       
        // Subscribe via POST request
        $response = $this->mailchimp->post("lists/{$list_id}/members", $vec_subscription_options);

        // Log response?
        if ( $this->is_test_mode )
        {
            Log::newsletter_dev("MailchimpManager::subscribe({$email}". print_r($response, true));
        }

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('subscribe', $response);

            return false;
        }

        // Save MailchimpSubscription model
        $this->save_mailchimp_subscription($response, $user_id);

        return $response;
    }


    /**
     * User will be subscribed or updated if he/she is already subscribed
     */
    public function subscribeOrUpdate($email, $vec_merge_fields = [], $list_name = '', $vec_options = [], $is_force_subscribe = false, $user_id = null)
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::subscribeOrUpdate() - List {$list_name} does not exist.");
            
            return false;
        }

        // Generate subscription options (request body parameters)
        $vec_subscription_options = $this->get_subscription_options($email, $vec_merge_fields, $vec_options);

        // Force subscription. It means, make the subscription to Mailchimp even if the suscriber has been UNSUBSCRIBED
        if ( ! $is_force_subscribe && isset($vec_subscription_options['status']) )
        {
            $vec_subscription_options['status_if_new'] = $vec_subscription_options['status'];
            unset($vec_subscription_options['status']);
        }

        // Subscribe or update via PUT request
        $response = $this->mailchimp->put("lists/{$list_id}/members/{$this->get_subscriber_hash($email)}", $vec_subscription_options);

        // Log response?
        if ( $this->is_test_mode )
        {
            Log::newsletter_dev("MailchimpManager::subscribeOrUpdate({$email}". print_r($response, true));
        }

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('subscribeOrUpdate', $response);

            return false;
        }

        // Tags must be added manually because "tags" are not included in endpoint PUT /lists/{list_id}/members/{subscriber_hash}.
        $vec_tags = [];
        if ( isset($vec_subscription_options['tags']) )
        {
            $vec_tags = $vec_subscription_options['tags'];
            $response_tags = $this->add_tags($vec_subscription_options['tags'], $email, $list_name);
        }

        // Save MailchimpSubscription model
        $this->save_mailchimp_subscription($response, $user_id, $vec_tags);

        return $response;
    }


    /**
     * Unsubscribe from Mailchimp
     */
    public function unsubscribe($email, $list_name = '', $user_id = null)
    {
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::unsubscribe() - List {$list_name} does not exist.");
            
            return false;
        }

        // Unsubscribe via PATCH request
        $response = $this->mailchimp->patch("lists/{$list_id}/members/{$this->get_subscriber_hash($email)}", [
            'status' => 'unsubscribed',
        ]);

        // Log response?
        if ( $this->is_test_mode )
        {
            Log::newsletter_dev("MailchimpManager::unsubscribe({$email}". print_r($response, true));
        }

        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('unsubscribe', $response);

            return false;
        }

        // Save MailchimpSubscription model
        $this->save_mailchimp_subscription($response, $user_id);

        return $response;
    }


    /**
     * Get Member information from Mailchimp
     */
    public function get_member($email, $list_name = '')
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::get_member() - List {$list_name} does not exist.");
            
            return false;
        }

        // Retrieve data via GET request
        return $this->mailchimp->get("lists/{$list_id}/members/{$this->get_subscriber_hash($email)}");
    }


    /**
     * Get all members from a Mailchimp list
     */
    public function get_list_members($list_name = '', $vec_parameters = [])
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::get_list_members() - List {$list_name} does not exist.");
            
            return false;
        }

        // Retrieve data via GET request
        $response = $this->mailchimp->get("lists/{$list_id}/members", $vec_parameters);

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('get_list_members', $response);

            return false;
        }

        return $response;
    }


    /**
     * Check if a list has a member in any status type: subscribed, unsubscried, pending
     */
    public function is_member($email, $list_name = '')
    {
        // Get member information
        $response = $this->get_member($email, $list_name);

        // Log response?
        if ( $this->is_test_mode )
        {
            Log::newsletter_dev("MailchimpManager::is_member({$email}". print_r($response, true));
        }

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('is_member', $response);

            return false;
        }

        if ( ! isset($response['email_address']) )
        {
            return false;
        }

        if ( StringHelper::strtolower($response['email_address']) != StringHelper::strtolower($email) )
        {
            return false;
        }

        return true;
    }


    /**
     * Check if a list has a SUBSCRIBED member
     */
    public function is_subscribed_member($email, $list_name)
    {
        // Get member information
        $response = $this->get_member($email, $list_name);

        // Log response?
        if ( $this->is_test_mode )
        {
            Log::newsletter_dev("MailchimpManager::is_subscribed_member({$email}". print_r($response, true));
        }

        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('is_subscribed_member', $response);

            return false;
        }

        if ( $response['status'] != 'subscribed')
        {
            return false;
        }

        return true;
    }


    /**
     * Update email address from an existing member
     */
    public function update_email_address($current_email_address, $new_email_address, $list_name = '')
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::update_email_address() - List {$list_name} does not exist.");
            
            return false;
        }

        // Update email address via PATCH request
        $response = $this->mailchimp->patch("lists/{$list_id}/members/{$this->get_subscriber_hash($current_email_address)}", [
            'email_address' => $new_email_address,
        ]);

        // Log response?
        if ( $this->is_test_mode )
        {
            Log::newsletter_dev("MailchimpManager::update_email_address({$current_email_address}, {$new_email_address})". print_r($response, true));
        }

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('update_email_address', $response);

            return false;
        }

        return $response;
    }


    /**
     * Delete a subscriber
     */
    public function delete($email, $list_name = '')
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::delete() - List {$list_name} does not exist.");
            
            return false;
        }

        // Delete subscriber via DELETE request
        $response = $this->mailchimp->delete("lists/{$list_id}/members/{$this->get_subscriber_hash($email)}");

        // Log response?
        if ( $this->is_test_mode )
        {
            Log::newsletter_dev("MailchimpManager::delete({$email}". print_r($response, true));
        }

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('delete', $response);

            return false;
        }

        return $response;
    }


    /**
     * Delete a subscriber permanently
     */
    public function delete_permanently($email, $list_name = '')
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::delete_permanently() - List {$list_name} does not exist.");
            
            return false;
        }

        // Delete subscriber via POST request
        $response = $this->mailchimp->post("lists/{$list_id}/members/{$this->get_subscriber_hash($email)}/actions/delete-permanent");

        // Log response?
        if ( $this->is_test_mode )
        {
            Log::newsletter_dev("MailchimpManager::delete_permanently({$email}". print_r($response, true));
        }

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('delete_permanently', $response);

            return false;
        }

        return $response;
    }


    /**
     * Return subscription options with default values
     */
    public function get_subscription_options($email, $vec_merge_fields, $vec_options)
    {
        $vec_default_options = [
            'email_address' => $email,
            'status'        => 'subscribed',
            'email_type'    => 'html',
        ];

        if ( !empty($vec_merge_fields) )
        {
            $vec_default_options['merge_fields'] = $vec_merge_fields;
        }

        return \CMap::mergeArray($vec_default_options, $vec_options);
    }


    /**
     * Return the MD5 hash code for a Mailchimp's subscriber
     */
    public function get_subscriber_hash($email)
    {
        return $this->mailchimp->subscriberHash($email);
    }


    /**
     * Return assigned tags for a member
     */
    public function get_member_tags($email, $list_name = '')
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::get_member_tags() - List {$list_name} does not exist.");
            
            return false;
        }

        // Get tags via GET request
        $response = $this->mailchimp->get("lists/{$list_id}/members/{$this->get_subscriber_hash($email)}/tags");

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('get_member_tags', $response);

            return false;
        }

        return $response;
    }


    /**
     * Add tags to a member
     */
    public function add_tags($vec_tags, $email, $list_name = '')
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::add_tags() - List {$list_name} does not exist.");
            
            return false;
        }

        $vec_input_tags = [];
        foreach ( $vec_tags as $que_tag )
        {
            $vec_input_tags[] = [
                'name'      => $que_tag,
                'status'    => 'active'
            ];
        }

        // Add tags via POST request
        $response = $this->mailchimp->post("lists/{$list_id}/members/{$this->get_subscriber_hash($email)}/tags", [
            'tags' => $vec_input_tags,
        ]);

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('add_tags', $response);

            return false;
        }

        return $response;
    }


    /**
     * Remove tags from a member
     */
    public function remove_tags($vec_tags, $email, $list_name = '')
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::remove_tags() - List {$list_name} does not exist.");
            
            return false;
        }

        $vec_input_tags = [];
        foreach ( $vec_tags as $que_tag )
        {
            $vec_input_tags[] = [
                'name'      => $que_tag,
                'status'    => 'inactive'
            ];
        }

        // Remove tags via POST request
        $response = $this->mailchimp->post("lists/{$list_id}/members/{$this->get_subscriber_hash($email)}/tags", [
            'tags' => $vec_input_tags,
        ]);

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('remove_tags', $response);

            return false;
        }

        return $response;
    }


    /**
     * Set tags from a member
     */
    public function set_tags($vec_tags, $email, $list_name = '')
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::set_tags() - List {$list_name} does not exist.");

            return false;
        }

        // Remove tags via POST request
        $response = $this->mailchimp->post("lists/{$list_id}/members/{$this->get_subscriber_hash($email)}/tags", [
            'tags' => $vec_tags,
        ]);

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('set_tags', $response);

            return false;
        }

        return $response;
    }


    /**
     * Create a Mailchimp campaign. NO EMAIL WILL BE SENT OUT!
     */
    public function create_campaign($from_name, $reply_to, $subject, $html = '', $list_name = '', $vec_options = [], $vec_content_options = [])
    {
        // Get list id
        $list_id = $this->get_list_id($list_name);
        if ( ! $list_id )
        {
            Log::newsletter("ERROR - MailchimpManager::create_campaign() - List {$list_name} does not exist.");
            
            return false;
        }

        $vec_default_options = [
            'type' => 'regular',
            'recipients' => [
                'list_id' => $list_id,
            ],
            'settings' => [
                'subject_line'  => $subject,
                'from_name'     => $from_name,
                'reply_to'      => $reply_to,
            ],
        ];

        $vec_options = \CMap::mergeArray($vec_default_options, $vec_options);

        $response = $this->mailchimp->post('campaigns', $vec_options);

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('create_campaign', $response);

            return false;
        }

        if ( $html === '' )
        {
            return $response;
        }

        if ( ! $this->update_content($response['id'], $html, $vec_content_options) )
        {
            return false;
        }

        return $response;
    }


    /**
     * Update the mail content of a Campaign
     */
    public function update_content($campaign_id, $html, $vec_options = [])
    {
        $vec_default_options = compact('html');

        $vec_options = \CMap::mergeArray($vec_default_options, $vec_options);

        $response = $this->mailchimp->put("campaigns/{$campaign_id}/content", $vec_options);

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('update_content', $response);

            return false;
        }

        return $response;
    }


    /**
     * Send a created campaign
     */
    public function send_campaign($campaign_id, $is_test = false, $vec_emails = [])
    {
        if ( ! $is_test )
        {
            $response = $this->mailchimp->post("campaigns/{$campaign_id}/actions/send");
        }
        else
        {
            $vec_options = [
                'test_emails'   => !empty($vec_emails) ? $vec_emails : [Yii::app()->params['mail']['test_email']],
                'send_type'     => 'html'
            ];
            $response = $this->mailchimp->post("campaigns/{$campaign_id}/actions/test", $vec_options);   
        }

        // Error detected
        if ( ! $this->is_last_action_success() )
        {
            // Register Mailchimp error
            $this->save_errors('send_campaign', $response);

            return false;
        }

        if ( ! $is_test )
        {
            Log::newsletter("Campaign #{$campaign_id} was sent succesfully.");
        }
        else if ( isset($vec_options['test_emails']))
        {
            Log::newsletter("A TEST for campaign #{$campaign_id} was sent succesfully to: ". implode(", ", $vec_options['test_emails']));
        }

        return $response;
    }


    /**
     * Sends a test email with the Mailchimp campaign content
     */
    public function send_test_campaign($campaign_id, $vec_emails = [])
    {
        return $this->send_campaign($campaign_id, true, $vec_emails);
    }


    /**
     * Return last Mailchimp error
     */
    public function get_last_error()
    {
        return $this->mailchimp->getLastError();
    }


    /**
     * Save errors into LOG
     */
    public function save_errors($action, $response)
    {
        $last_error = $this->get_last_error();

        // Log error
        Log::newsletter("ERROR from Mailchimp: {$last_error} - MailchimpManager::{$action}()");

        // For testing save full response
        if ( $this->is_test_mode )
        {
            Log::newsletter_dev("ERROR from Mailchimp: {$last_error} - MailchimpManager::{$action}() - Response: ". print_r($response, true));
        }
    }


    /**
     * Check if last Mailchimp action was succedded
     */
    public function is_last_action_success()
    {
        return $this->mailchimp->success();
    }


    /**
     * Save MailchimpSubscription model
     */
    public function save_mailchimp_subscription($response, $user_id = null, $vec_tags = [])
    {
        // Check if it already exists a subscription for this email and list
        $mailchimp_subscription_model = MailchimpSubscription::get()
            ->where([
                'email' => $response['email_address'],
                'mailchimp_list_id' => $response['list_id'],
            ])
            ->one();

        // Create new MailchimpSubscription model
        if ( ! $mailchimp_subscription_model )
        {
            $mailchimp_subscription_model = Yii::createObject(MailchimpSubscription::class);
            $mailchimp_subscription_model->setAttributes([
                'email' => $response['email_address'],
                'mailchimp_list_id' => $response['list_id'],
            ]);
        }

        // Get data center prefix for the URL
        $data_center = $this->get_data_center();

        // Update attributes
        $mailchimp_subscription_model->setAttributes([
            'mailchimp_email_id'    => $response['unique_email_id'],
            'mailchimp_hash'        => $response['id'],
            'mailchimp_web_id'      => $response['web_id'],
            'mailchimp_url'         => !empty($data_center) ? 'https://'. $data_center .'.admin.mailchimp.com/lists/members/view?id='. $response['web_id'] : 'https://admin.mailchimp.com/lists/members/view?id='. $response['web_id'],
        ]);

        // Is an user related with this subscription?
        if ( $user_id !== null )
        {
            $mailchimp_subscription_model->user_id = $user_id;
        }

        // Status
        $vec_allowed_statuses = $mailchimp_subscription_model->status_type_labels();
        if ( !empty($vec_allowed_statuses) && isset($vec_allowed_statuses[$response['status']]) )
        {
            $mailchimp_subscription_model->status_type = $response['status'];
        }

        // Merge fields
        if ( isset($response['merge_fields']) )
        {
            $mailchimp_subscription_model->mailchimp_merge_fields = Json::encode($response['merge_fields']);
        }

        // Add new tags?
        if ( !empty($vec_tags) )
        {
            // Recover current tags
            if ( !empty($mailchimp_subscription_model->mailchimp_tags) )
            {
                $vec_current_tags = explode(",", $mailchimp_subscription_model->mailchimp_tags);
                if ( !empty($vec_current_tags) )
                {
                    foreach ( $vec_current_tags as $current_tag )
                    {
                        if ( !in_array($current_tag, $vec_tags) )
                        {
                            $vec_tags[] = $current_tag;
                        }
                    }
                }
            }

            // Finally, save the tags
            $mailchimp_subscription_model->mailchimp_tags = implode(',', $vec_tags);
        }

        // Save the model
        if ( ! $mailchimp_subscription_model->save() )
        {
            Log::save_model_error($mailchimp_subscription_model);

            return false;
        }

        return true;
    }
}
