<?php
/**
 * Newsletter configuration
 * 
 * Load it via Yii::app()->config->get('components.newsletter')
 */
return [

    // Mailchimp configuration
    'mailchimp' => [

        // The API key of a MailChimp account. You can find yours at
        // https://us10.admin.mailchimp.com/account/api-key-popup/.
        'api_key' => getenv('MAILCHIMP_APIKEY'),

        // The listName to use when no listName has been specified in a method.
        'default_list_name' => 'default',

        // Mailchimp lists
        'lists' => [

            // Custom name for the list. You can set it to any string you want and you can add
            'default' => [

                // A MailChimp list id.
                // Check the MailChimp docs if you don't know how to get this value: http://kb.mailchimp.com/lists/managing-subscribers/find-your-list-id.
                'id' => getenv('MAILCHIMP_LIST_ID'),
            ],
        ],

        // If you're having trouble with https connections, set this to false.
        'ssl' => true,

        // Test mode? Log all responses
        'is_test_mode' => false
    ]
];
