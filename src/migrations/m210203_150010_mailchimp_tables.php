<?php
/**
 * Migration class m210203_150010_mailchimp_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210203_150010_mailchimp_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Create "mailchimp_subscription" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('mailchimp_subscription', true);

        $this->createTable('mailchimp_subscription', [
            'subscription_id' => $this->primaryKey(),
            'email' => $this->string(255)->notNull(),
            'mailchimp_list_id' => $this->string(16)->notNull(),
            'status_type' => $this->enum('status_type', ['subscribed', 'unsubscribed', 'cleaned', 'pending', 'transactional', 'archived'])->defaultValue('subscribed'),
            'user_id' => $this->integer()->unsigned(),
            'mailchimp_email_id' => $this->string(32)->notNull(),
            'mailchimp_hash' => $this->string(32)->notNull(),
            'mailchimp_web_id' => $this->integer()->unsigned()->notNull(),
            'mailchimp_url' => $this->string(255),
            'mailchimp_merge_fields' => $this->string(512),
            'mailchimp_tags' => $this->string(255),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'mailchimp_subscription', ['email'], false);
        $this->createIndex(null, 'mailchimp_subscription', ['mailchimp_email_id'], false);
        $this->createIndex(null, 'mailchimp_subscription', ['mailchimp_list_id'], false);
        $this->createIndex(null, 'mailchimp_subscription', ['mailchimp_list_id', 'status_type'], false);
        $this->createIndex(null, 'mailchimp_subscription', ['mailchimp_web_id'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'mailchimp_subscription', ['user_id'], 'user_users', ['id'], 'SET NULL', null);
        $this->addForeignKey(null, 'mailchimp_subscription', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'mailchimp_subscription', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "mailchimp_campaign" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('mailchimp_campaign', true);

        $this->createTable('mailchimp_campaign', [
            'campaign_id' => $this->primaryKey(),
            'mailchimp_id' => $this->string(16),
            'mailchimp_web_id' => $this->integer()->unsigned(),
            'mailchimp_url' => $this->string(255),
            'subject' => $this->string(255),
            'body' => $this->text(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
    
        // Create indexes
        $this->createIndex(null, 'mailchimp_campaign', ['mailchimp_id'], false);
        $this->createIndex(null, 'mailchimp_campaign', ['mailchimp_web_id'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'mailchimp_campaign', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'mailchimp_campaign', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

