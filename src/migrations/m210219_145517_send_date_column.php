<?php
/**
 * Migration class m210219_145517_send_date_column
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210219_145517_send_date_column extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// New column "send_date"
		$this->addColumn('mailchimp_campaign', 'send_date', $this->date()->after('body'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

