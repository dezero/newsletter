<?php
/**
 * @package dzlab\newsletter\models 
 */

namespace dzlab\newsletter\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\newsletter\models\_base\MailchimpCampaign as BaseMailchimpCampaign;
use user\models\User;
use Yii;

/**
 * MailchimpCampaign model class for "mailchimp_campaign" database table
 *
 * Columns in table "mailchimp_campaign" available as properties of the model,
 * followed by relations of table "mailchimp_campaign" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $campaign_id
 * @property string $mailchimp_id
 * @property integer $mailchimp_web_id
 * @property string $mailchimp_url
 * @property string $subject
 * @property string $body
 * @property integer $send_date
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $updatedUser
 */
class MailchimpCampaign extends BaseMailchimpCampaign
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['created_date, created_uid, updated_date, updated_uid', 'required'],
			['mailchimp_web_id, send_date, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['mailchimp_id', 'length', 'max'=> 16],
			['mailchimp_url, subject', 'length', 'max'=> 255],
			['uuid', 'length', 'max'=> 36],
			['mailchimp_id, mailchimp_web_id, mailchimp_url, subject, body, send_date, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['body', 'safe'],
			['campaign_id, mailchimp_id, mailchimp_web_id, mailchimp_url, subject, body, send_date, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'send_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'campaign_id' => Yii::t('app', 'Campaign'),
			'mailchimp_id' => Yii::t('app', 'Mailchimp Id'),
			'mailchimp_web_id' => Yii::t('app', 'Mailchimp Web Id'),
			'mailchimp_url' => Yii::t('app', 'Mailchimp URL'),
			'subject' => Yii::t('app', 'Subject'),
			'body' => Yii::t('app', 'Body'),
            'send_date' => Yii::t('app', 'Sent Date'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.campaign_id', $this->campaign_id);
        $criteria->compare('t.mailchimp_id', $this->mailchimp_id, true);
        $criteria->compare('t.mailchimp_web_id', $this->mailchimp_web_id);
        $criteria->compare('t.mailchimp_url', $this->mailchimp_url, true);
        $criteria->compare('t.subject', $this->subject, true);
        $criteria->compare('t.body', $this->body, true);
        $criteria->compare('t.send_date', $this->send_date);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['campaign_id' => true]]
        ]);
    }


    /**
     * MailchimpCampaign models list
     * 
     * @return array
     */
    public function mailchimpcampaign_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['campaign_id', 'uuid'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = MailchimpCampaign::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('campaign_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Send the campaign via Mailchimp API
     */
    public function send($is_test = false)
    {
        if ( !empty($this->mailchimp_id) )
        {
            // Send the campaign
            if ( ! $is_test )
            {
                $mailchimp_response = Yii::app()->mailchimp->send_campaign($this->mailchimp_id);
            }

            // Send a tests
            else
            {
                $mailchimp_response = Yii::app()->mailchimp->send_test_campaign($this->mailchimp_id);
            }
            

            if ( $mailchimp_response === false )
            {
                return false;
            }
            else if ( ! $is_test )
            {
                $this->send_date = date('d/m/Y - H:i');
                $this->raw_attributes['send_date'] = time();
                $this->saveAttributes([
                    'send_date' => $this->raw_attributes['send_date']
                ]);
            }

            return true;
        }

        return false;
    }


    /**
     * Send a test email with the Mailchimp campaign content
     */ 
    public function send_test()
    {
        return $this->send(true);
    }
}