<?php
/**
 * @package dzlab\newsletter\models\_base
 */

namespace dzlab\newsletter\models\_base;

use dz\db\ActiveRecord;
use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use user\models\User;
use Yii;

/**
 * DO NOT MODIFY THIS FILE! It is automatically generated by Gii.
 * If any changes are necessary, you must set or override the required
 * property or method in class "dzlab\newsletter\models\MailchimpCampaign".
 *
 * This is the model base class for the table "mailchimp_campaign".
 * Columns in table "mailchimp_campaign" available as properties of the model,
 * followed by relations of table "mailchimp_campaign" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $campaign_id
 * @property string $mailchimp_id
 * @property integer $mailchimp_web_id
 * @property string $mailchimp_url
 * @property string $subject
 * @property string $body
 * @property integer $send_date
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $updatedUser
 */
abstract class MailchimpCampaign extends ActiveRecord
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * Returns the name of the associated database table
	 */
	public function tableName()
	{
		return 'mailchimp_campaign';
	}


	/**
	 * Label with translation support (from GIIX)
	 */
	public static function label($n = 1)
	{
		return Yii::t('app', 'MailchimpCampaign|MailchimpCampaigns', $n);
	}


    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['created_date, created_uid, updated_date, updated_uid', 'required'],
            ['mailchimp_web_id, send_date, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
            ['mailchimp_id', 'length', 'max'=> 16],
            ['mailchimp_url, subject', 'length', 'max'=> 255],
            ['uuid', 'length', 'max'=> 36],
            ['mailchimp_id, mailchimp_web_id, mailchimp_url, subject, body, send_date, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
            ['body', 'safe'],
            ['campaign_id, mailchimp_id, mailchimp_web_id, mailchimp_url, subject, body, send_date, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
        ];
    }


    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
        ];
    }


    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'campaign_id' => Yii::t('app', 'Campaign'),
            'mailchimp_id' => Yii::t('app', 'Mailchimp'),
            'mailchimp_web_id' => Yii::t('app', 'Mailchimp Web'),
            'mailchimp_url' => Yii::t('app', 'Mailchimp Url'),
            'subject' => Yii::t('app', 'Subject'),
            'body' => Yii::t('app', 'Body'),
            'send_date' => Yii::t('app', 'Send Date'),
            'created_date' => Yii::t('app', 'Created Date'),
            'created_uid' => null,
            'updated_date' => Yii::t('app', 'Updated Date'),
            'updated_uid' => null,
            'uuid' => Yii::t('app', 'Uuid'),
            'createdUser' => null,
            'updatedUser' => null,
        ];
    }


    /**
     * MailchimpCampaign models list
     * 
     * @return array
     */
    public function mailchimpcampaign_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['campaign_id', 'uuid'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = MailchimpCampaign::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('campaign_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


	/**
	 * Generate an ActiveDataProvider for search form of this model
	 *
	 * Used in CGridView
	 */
	public function search()
	{
		$criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

		$criteria->compare('t.campaign_id', $this->campaign_id);
		$criteria->compare('t.mailchimp_id', $this->mailchimp_id, true);
		$criteria->compare('t.mailchimp_web_id', $this->mailchimp_web_id);
		$criteria->compare('t.mailchimp_url', $this->mailchimp_url, true);
		$criteria->compare('t.subject', $this->subject, true);
		$criteria->compare('t.body', $this->body, true);
		$criteria->compare('t.created_date', $this->created_date);
		$criteria->compare('t.updated_date', $this->updated_date);
		$criteria->compare('t.uuid', $this->uuid, true);

		return new \CActiveDataProvider($this, [
			'criteria' => $criteria,
			'pagination' => ['pageSize' => 30],
			'sort' => ['defaultOrder' => ['campaign_id' => true]]
		]);
	}
	
	
	/**
	 * Returns fields not showed in create/update forms
	 */
	public function excludedFormFields()
	{
		return [
			'created_date' => true, 
			'created_uid' => true, 
			'updated_date' => true, 
			'updated_uid' => true, 
		];
	}


    /**
     * Automatic string representation for a model (from GIIX)
     */
    public static function representingColumn()
    {
        return 'uuid';
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->uuid;
    }
}