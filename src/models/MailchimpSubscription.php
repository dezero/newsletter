<?php
/**
 * @package dzlab\newsletter\models 
 */

namespace dzlab\newsletter\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\newsletter\models\_base\MailchimpSubscription as BaseMailchimpSubscription;
use user\models\User;
use Yii;

/**
 * MailchimpSubscription model class for "mailchimp_subscription" database table
 *
 * Columns in table "mailchimp_subscription" available as properties of the model,
 * followed by relations of table "mailchimp_subscription" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $subscription_id
 * @property string $email
 * @property string $mailchimp_list_id
 * @property string $status_type
 * @property integer $user_id
 * @property string $mailchimp_email_id
 * @property string $mailchimp_hash
 * @property integer $mailchimp_web_id
 * @property string $mailchimp_url
 * @property string $mailchimp_merge_fields
 * @property string $mailchimp_tags
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $updatedUser
 * @property mixed $user
 */
class MailchimpSubscription extends BaseMailchimpSubscription
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['email, mailchimp_list_id, mailchimp_email_id, mailchimp_hash, mailchimp_web_id, created_date, created_uid, updated_date, updated_uid', 'required'],
			['user_id, mailchimp_web_id, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['email, mailchimp_url, mailchimp_tags', 'length', 'max'=> 255],
			['mailchimp_list_id', 'length', 'max'=> 16],
			['mailchimp_email_id, mailchimp_hash', 'length', 'max'=> 32],
            ['mailchimp_merge_fields', 'length', 'max'=> 512],
			['uuid', 'length', 'max'=> 36],
			['status_type', 'in', 'range' => ['subscribed', 'unsubscribed', 'cleaned', 'pending', 'transactional', 'archived']],
			['status_type, user_id, mailchimp_url, mailchimp_merge_fields, mailchimp_tags, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['subscription_id, email, mailchimp_list_id, status_type, user_id, mailchimp_email_id, mailchimp_hash, mailchimp_web_id, mailchimp_url, mailchimp_merge_fields, mailchimp_tags, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
			'user' => [self::BELONGS_TO, User::class, 'user_id'],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'subscription_id' => Yii::t('app', 'Subscription'),
			'email' => Yii::t('app', 'Email'),
			'mailchimp_list_id' => Yii::t('app', 'Mailchimp List'),
			'status_type' => Yii::t('app', 'Status Type'),
			'user_id' => Yii::t('app', 'User'),
            'mailchimp_email_id' => Yii::t('app', 'Mailchimp Email'),
			'mailchimp_hash' => Yii::t('app', 'Mailchimp Hash'),
			'mailchimp_web_id' => Yii::t('app', 'Mailchimp Web'),
			'mailchimp_url' => Yii::t('app', 'Mailchimp Url'),
            'mailchimp_merge_fields' => Yii::t('app', 'Mailchimp Merge Fields'),
			'mailchimp_tags' => Yii::t('app', 'Mailchimp Tags'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'updatedUser' => null,
			'user' => null,
		];
	}


    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        return [
            'subscribed' => Yii::t('app', 'subscribed'),
            'unsubscribed' => Yii::t('app', 'unsubscribed'),
            'cleaned' => Yii::t('app', 'cleaned'),
            'pending' => Yii::t('app', 'pending'),
            'transactional' => Yii::t('app', 'transactional'),
            'archived' => Yii::t('app', 'archived'),
        ];
    }


    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type)
    {
        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.subscription_id', $this->subscription_id);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.mailchimp_list_id', $this->mailchimp_list_id, true);
        $criteria->compare('t.status_type', $this->status_type, true);
        $criteria->compare('t.mailchimp_email_id', $this->mailchimp_email_id, true);
        $criteria->compare('t.mailchimp_hash', $this->mailchimp_hash, true);
        $criteria->compare('t.mailchimp_web_id', $this->mailchimp_web_id);
        $criteria->compare('t.mailchimp_url', $this->mailchimp_url, true);
        $criteria->compare('t.mailchimp_merge_fields', $this->mailchimp_merge_fields, true);
        $criteria->compare('t.mailchimp_tags', $this->mailchimp_tags, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['subscription_id' => true]]
        ]);
    }


    /**
     * MailchimpSubscription models list
     * 
     * @return array
     */
    public function mailchimpsubscription_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['subscription_id', 'email'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = MailchimpSubscription::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('subscription_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Unsubscribe action -> Update Mailchimp subscription status field
     */
    public function unsubscribe()
    {
        $mailchimp_response = Yii::app()->mailchimp->unsubscribe($this->email, 'default', $this->user_id);
        if ( $mailchimp_response )
        {
            Log::newsletter('Member '. $this->email .': Unsubscribed');
        }

        return $mailchimp_response;
    }


    /**
     * Check if email or merge fields have been changed from the last subscription
     */
    public function is_subscription_changed($new_email, $vec_merge_fields = [])
    {
        $json_merge_fields = Json::encode($vec_merge_fields);

        return ( $new_email !== $this->email || $this->mailchimp_merge_fields !== $json_merge_fields );
    }
}