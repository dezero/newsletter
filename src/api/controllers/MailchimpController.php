<?php
/*
|--------------------------------------------------------------------------
| Controller class for managing Mailchimp subscriptions
|--------------------------------------------------------------------------
*/

namespace dzlab\newsletter\api\controllers;

use dzlab\newsletter\api\resources\MailchimpResource;
use dz\rest\RestController;
use Yii;

class MailchimpController extends RestController
{
    /**
     * Main action for Mailchimp subscriptions
     *
     *   + POST method: api/v1/mailchimp
     *
     * @return JSON
     */
    public function actionIndex()
    {
        $mailchimp_resource = Yii::createObject(MailchimpResource::class);

        // #1 - VALIDATE INPUT PARAMS
        if ( $mailchimp_resource->validate('index') )
        {
            // #2  - PROCESS REQUEST
            switch ( $mailchimp_resource->request_type )
            {
                // Subscribe email to Mailchimp via POST method: api/v1/mailchimp
                case 'POST':
                    $mailchimp_resource->post_subscribe();
                break;
            }
        }

        // #3 - SEND REST response
        $mailchimp_resource->send_response();
    }


    /**
     * Item action: api/v1/mailchimp/xxx
     *
     * @return JSON
     */
    public function actionItem($id)
    {
        // POST method: api/v1/mailchimp/unsubscribe
        if ( $id == 'unsubscribe' )
        {
            return $this->actionUnsubscribe();
        }
    }


    /**
     * Unsubscribe email from Mailchimp
     *
     *   + POST method: api/v1/mailchimp/unsubscribe
     *
     * @return JSON
     */
    public function actionUnsubscribe()
    {
        $mailchimp_resource = Yii::createObject(MailchimpResource::class);

        // #1 - VALIDATE INPUT PARAMS
        if ( $mailchimp_resource->validate('unsubscribe') )
        {
            // #2  - PROCESS REQUEST
            switch ( $mailchimp_resource->request_type )
            {
                // POST method: api/v1/mailchimp/unsubscribe
                case 'POST':
                    $mailchimp_resource->post_unsubscribe();
                break;
            }
        }

        // #3 - SEND REST response
        $mailchimp_resource->send_response();
    }
}
