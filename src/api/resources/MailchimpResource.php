<?php
/*
|--------------------------------------------------------------------------
| Subscriptions Resource class for REST API
|--------------------------------------------------------------------------
*/

namespace dzlab\newsletter\api\resources;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\rest\RestResource;
use user\models\User;
use Yii;

class MailchimpResource extends RestResource
{
    /**
     * Validate input params
     */
    public function validate($action = 'index')
    {
        if ( ! $this->is_validated )
        {
            if ( $action === 'index' )
            {
                switch ( $this->request_type )
                {
                    // Subscribe email to Mailchimp via POST method: api/v1/mailchimp
                    case 'POST':
                        $this->_validate_post();
                    break;
                }
            }

            // Unsubscribe email from Mailchimp - POST method: api/v1/mailchimp/unsubscribe
            else if ( $action === 'unsubscribe' && $this->request_type == 'POST' )
            {
                $this->_validate_unsubscribe();
            }
        }

        return $this->is_validated;
    }


    /**
     * Subscribe email to Mailchimp
     */
    public function post_subscribe()
    {
        if ( $this->request_type == 'POST' )
        {
            // Prepare common REST API output
            $this->vec_response = [
                'status_code'           => 1,
                'errors'                => [],
                'mailchimp_response'    => [],
            ];

            // Check MERGE fields
            $vec_merge_fields = [];
            if ( isset($this->vec_input['merge_fields']) && !empty($this->vec_input['merge_fields']) && is_array($this->vec_input['merge_fields']) )
            {
                $vec_merge_fields = $this->vec_input['merge_fields'];
            }

            // List ID
            $list_id = ( isset($this->vec_input['list_id']) && !empty($this->vec_input['list_id']) ) ? $this->vec_input['list_id'] : 'default';

            // Options
            $vec_options = [];
            if ( isset($this->vec_input['options']) && !empty($this->vec_input['options']) )
            {
                $vec_options = $this->vec_input['options'];
            }

            // Force subscription. It means, make the subscription to Mailchimp even if the suscriber has been UNSUBSCRIBED
            $is_force_subscribe = true;

            // Make the subscription via Mailchimp API
            $mailchimp_response = Yii::app()->mailchimp->subscribeOrUpdate($this->vec_input['email'], $vec_merge_fields, $list_id, $vec_options, $is_force_subscribe);
            if ( ! $mailchimp_response )
            {
                $mailchimp_error = Yii::app()->mailchimp->get_last_error();
                if ( !empty($mailchimp_error) )
                {
                    $this->send_error(103, "Email could not be subscribed to Mailchimp: {$mailchimp_error}");
                }
                else
                {
                    $this->send_error(103, 'Email could not be subscribed to Mailchimp');
                }
            }
            else
            {
                Log::newsletter('Email '. $this->vec_input['email'] .' has been subscribed or updated to Mailchimp');

                $this->vec_response['mailchimp_response'] = $mailchimp_response;
            }

            return true;
        }

        return false;
    }


    /**
     * Unsubscribe email to Mailchimp
     */
    public function post_unsubscribe()
    {
        if ( $this->request_type == 'POST' )
        {
            // Prepare common REST API output
            $this->vec_response = [
                'status_code'           => 1,
                'errors'                => [],
                'mailchimp_response'    => [],
            ];

            // List ID
            $list_id = ( isset($this->vec_input['list_id']) && !empty($this->vec_input['list_id']) ) ? $this->vec_input['list_id'] : 'default';


            $mailchimp_response = Yii::app()->mailchimp->unsubscribe($this->vec_input['email'], $list_id);
            if ( ! $mailchimp_response )
            {
                $mailchimp_error = Yii::app()->mailchimp->get_last_error();
                if ( !empty($mailchimp_error) )
                {
                    $this->send_error(103, "Email could not be unsubscribed from Mailchimp: {$mailchimp_error}");
                }
                else
                {
                    $this->send_error(103, 'Email could not be unsubscribed from Mailchimp');
                }
            }
            else
            {
                Log::newsletter('Email '. $this->vec_input['email'] .' has been unsubscribed from Mailchimp');

                $this->vec_response['mailchimp_response'] = $mailchimp_response;
            }

            return true;
        }

        return false;
    }


    /**
     * Validate input params for POST /api/v1/mailchimp endpoint
     */
    private function _validate_post()
    {
        if ( ! $this->is_validated )
        {
            // Required parameter
            $vec_required_errors = [];
            $vec_required_params = ['email'];
            foreach ( $vec_required_params as $que_param )
            {
                if ( !isset($this->vec_input[$que_param]) )
                {
                    $vec_required_errors[] = 'Missing required parameter \''. $que_param .'\'';
                }
            }

            if ( !empty($vec_required_errors) )
            {
                $this->send_error(102, $vec_required_errors);
                return false;
            }

            // Set input as validated
            $this->is_validated = true;
        }

        return $this->is_validated;
    }



    /**
     * Validate input params for POST /api/v1/mailchimp/unsubscribe endpoint
     */
    private function _validate_unsubscribe()
    {
        if ( ! $this->is_validated )
        {
            // Required parameter
            $vec_required_errors = [];
            $vec_required_params = ['email'];
            foreach ( $vec_required_params as $que_param )
            {
                if ( !isset($this->vec_input[$que_param]) )
                {
                    $vec_required_errors[] = 'Missing required parameter \''. $que_param .'\'';
                }
            }

            if ( !empty($vec_required_errors) )
            {
                $this->send_error(102, $vec_required_errors);
                return false;
            }

            // Set input as validated
            $this->is_validated = true;
        }

        return $this->is_validated;
    }

}
